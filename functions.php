<?php

function generateSignature($username, $time)
{
    $salt = '11wwsdee333';
    return sha1($username . $time . $salt);
}