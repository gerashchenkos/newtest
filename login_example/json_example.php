<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "config.php";

//create json file
/*$users = [
    ["login" => "login1", "password" => '1'],
    ["login" => "login2", "password" => '2'],
    ["login" => "login3", "password" => '3']
];
file_put_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "hm11" . DIRECTORY_SEPARATOR . "users.json", json_encode($users));
*/

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $dirName = ROOT_PATH . DIRECTORY_SEPARATOR . "hm11" . DIRECTORY_SEPARATOR;
    if (file_exists($dirName . "users.json")) {
        $users = file_get_contents($dirName . "users.json");
        $users = json_decode($users, true);
        foreach($users as $user) {
            if ($user['login'] == $_POST['login'] && $user['password'] == $_POST['password']) {
                if (file_exists($dirName . $user['login'] . ".json")) {
                    $fLogin = file_get_contents($dirName . $user['login'] . ".json");
                    $fLogin = json_decode($fLogin, true);
                    $fLogin['logins'] = $fLogin['logins'] + 1;
                    file_put_contents($dirName . $user['login'] . ".json",
                                      json_encode($fLogin));
                } else {
                    file_put_contents($dirName . $user['login'] . ".json",
                                      json_encode(["logins" => 1]));
                }
            }
        }
    }
}


