<?php
require_once "config.php";

const WEIGHT_INDEX = 2.205;
const HEIGHT_INDEX = 39.37;

function calculateIMT(float $weight, int $height): float
{
    return ($weight / WEIGHT_INDEX) / (pow($height / HEIGHT_INDEX, 2));
}

if (!empty($_POST['weight']) && !empty($_POST['height'])) {
    $imt = calculateIMT($_POST['weight'], $_POST['height']);
}

//$imt = calculateIMT();
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "imt.php"
?>

