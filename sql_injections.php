<?php
declare(strict_types = 1);
require_once "config.php";

if(!empty($_POST)) {
    //$login = "test';DELETE FROM users_old -- habrahabra";
    $login = $_POST['login'];
    $pass = $_POST['password'];
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $pdo->prepare("
        SELECT
            `id`,
            `name`
        FROM
            `users`
        WHERE
            `name` = :name and `password` = :password
    "
    );
    $stmt->execute(["name" => $login, "password" => $pass]);
    $result = $stmt->fetch();
    if (!empty($result)) {
        echo "you are logged!";
    } else {
        echo "Login or password is invalid!";
    }
}
?>
<html>
<body>
<?php if (!empty($_GET['error'])): ?>
    <h3>Login or Password is invalid!</h3>
<?php endif; ?>
<form action="sql_injections.php" method="post">
    <p>
        <label>Login</label>
        <input type="text" name="login" value="">
    </p>
    <p>
        <label>Password</label>
        <input type="password" name="password" value="">
    </p>
    <div><input type="checkbox" name="remember"
                id="remember"/>
        <label for="remember-me">Remember me</label>
    </div>
    <input type="submit" value="Sing In">
</form>
</body>
</html>
