<?php

namespace ShopClasses\Interfaces;

interface Block
{
    public function block(User $user): void;
}