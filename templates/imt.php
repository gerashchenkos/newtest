<?php
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR .
                    "partials" . DIRECTORY_SEPARATOR . "header.php";
?>
<div class="main_page">
    <h1>Calculate IMT</h1>
    <div>
        <form method="POST" action="/imt.php">
            Weight: <input type="text" name="weight"/><br>
            Height: <input type="text" name="height"/><br>
            <input type="submit" value="Calculate">
        </form>
    </div>
    <?php if(isset($imt)): ?>
    <h3>IMT: <?php echo $imt; ?></h3>
    <?php endif; ?>
</div>
<?php
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR .
    "partials" . DIRECTORY_SEPARATOR . "footer.php";
?>